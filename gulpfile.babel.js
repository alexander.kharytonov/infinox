import gulp from 'gulp';
import gulpStats from 'gulp-stats';
import gulpUsage from 'gulp-help';

import requireDir from 'require-dir';

gulpStats(gulpUsage(gulp));

requireDir('gulp/tasks', {
  recurse: true
});
