$(document).ready(function() {
  var isAndroid = navigator.userAgent.toLowerCase().indexOf('android') > -1,
      isIOs = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
      windowHeight = $(window).height();

  function rippleButtonEffect(event) {
    var button = event.target,
        clientRect = button.getBoundingClientRect(),
        ripple = button.querySelector('.ripple');

    if (ripple) $(ripple).remove();

    ripple = document.createElement('span');
    ripple.className = 'ripple';
    ripple.style.height = ripple.style.width = parseInt(Math.max(clientRect.width, clientRect.height)) + 'px';
    button.appendChild(ripple);

    var top = parseInt(event.pageY - clientRect.top - ripple.offsetHeight / 2 - window.pageYOffset),
        left = parseInt(event.pageX - clientRect.left - ripple.offsetWidth / 2 - window.pageXOffset);

    ripple.style.top = top + 'px';
    ripple.style.left = left + 'px';
  };

  $(document).on('click', '.button', function(event) {
    rippleButtonEffect(event);
  });

  var carouselEl = $('[data-function="carousel"]');

  carouselEl.each(function() {
    var _this = $(this),
        options = {
          nav: false,
          lazyLoad: true,
          responsive: {
            0: {
              items: 1
            },
            640: {
              items: 2
            },
            960: {
              items: 3
            }
          },
          onInitialize: function() {
            _this.removeClass('onload');
          }
        };

    _this.owlCarousel(options);
  });

  var parallaxEl = $('[data-function="jarallax"]');

  parallaxEl.each(function() {
    var _this = $(this),
        options = {
          imgSrc: _this.data('poster'),
          type: _this.data('type') ? _this.data('type') : 'scroll',
          speed: (isAndroid || isIOs) ? .5 : _this.data('speed') ? _this.data('speed') : .5,
          noAndroid: _this.data('ios') ? _this.data('ios') : true,
          noIos: _this.data('ios') ? _this.data('ios') : true,
          zIndex: -1,
          onInit: function() {
            _this.removeClass('loading');
          }
        };

    if (isAndroid || isIOs) {
      _this.append('<div class="parallax--container" style="background-image:url('+ _this.data('poster') +')"></div>');
    }

    if (isAndroid && _this.hasClass('fullscreen') || isIOs && _this.hasClass('fullscreen')) {
      _this.css({
          minHeight: windowHeight
      });
    }

    if (_this.data('video')) {
      options.videoSrc = _this.data('video');
    }

    _this.jarallax(options);
  });

  var AOSOptions = {
    easing: 'ease-out-sine',
    duration: 500,
    offset: (isAndroid || isIOs) ? 25 : 125,
    once: true
  };

  AOS.init(AOSOptions);
});
