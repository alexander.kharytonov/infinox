# INFINOX Trading Power

![INFINOX Trading Power](https://api.monosnap.com/rpc/file/download?id=MuLQPJdHRhBDtvGo0ppZgettU7OEft)

## Installation
Run `npm install` for install package.

## Development server

Run `npm start` or `gulp webserver` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.
