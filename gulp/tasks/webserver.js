import gulp from 'gulp';
import connect from 'gulp-connect';
 
import config from '../config';

gulp.task('webserver', 'Web development server', ['watch'], function() {
  connect.server({
    host: 'localhost',
    port: 4200,
    root: [
      config.webroot
    ],
    livereload: true,
    debug: true
  });
});
