import gulp from 'gulp';
import iconfont from 'gulp-iconfont';
import iconfontCss from 'gulp-iconfont-css';

import config from '../config';

const runTimeStamp = Math.round(Date.now() / 1000);

gulp.task('webfont', 'Build ico bundle', () => {
  return gulp.src([config.ico.src])
    .pipe(iconfontCss({
      fontName: config.ico.fontName,
      path: config.ico.dist + 'template.less',
      targetPath: config.ico.fontName + '.less',
      cssClass: config.ico.fontName,
      debug: true
    }))
    .pipe(iconfont({
      fontName: config.ico.fontName,
      formats: ['ttf', 'eot', 'woff', 'woff2'],
      prependUnicode: true,
      startUnicode: 0xE600,
      normalize: true,
      fontHeight: 1000,
      timestamp: runTimeStamp,
      debug: true
    }))
    .pipe(gulp.dest(config.ico.dist + 'bundle/'));
});
