const basePath = {
  modules: './node_modules/',
  root: './dist/',
  src: './src/',
  dist: './dist/build/'
};

const configPath = {
  webroot: basePath.root,
  modules: basePath.modules,
  ico: {
    fontName: 'ico',
    src: basePath.src + 'ico/*.svg',
    dist: basePath.src + 'ico/',
  },
  styles: {
    src: basePath.src + 'styles/',
    dist: basePath.dist + 'css/'
  },
  scripts: {
    src: basePath.src + 'scripts/',
    dist: basePath.dist + 'js/'
  }
};

export default configPath;
